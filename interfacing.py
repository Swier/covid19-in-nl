import base64
import requests
import json
import logging
import time
from configfile import imgur_auth, slackhook
import pandas as pd
import matplotlib.pyplot as plt


def retry(func, check_func, retries=None, sleep=30, **kwargs):
    data = func(**kwargs)
    if check_func(data):
        return data
    
    while retries != 0:
        logging.warning(f"function {func.__name__} failed, retrying {retries} more times")
        time.sleep(sleep)
        
        data = func(**kwargs)
        if check_func(data):
            return data
        
        retries = retries and retries - 1 or retries

    return None

def check_rivm_data(df):
    most_recent_entry = df.Date_of_publication.max()
    if pd.to_datetime("today").date() == most_recent:
        return True

    if (datetime.datetime.today().time() < datetime.time(15, 15, 0)) and (
        (pd.to_datetime("today").date() - pd.to_timedelta(1, "d")).date == most_recent
    ):
        return True
    return False


def get_rivm_data(cache_dir='data'):
    try:
        df = pd.read_csv(
            "https://data.rivm.nl/covid-19/COVID-19_aantallen_gemeente_per_dag.csv",
            sep=";",
            parse_dates=["Date_of_publication", "Date_of_report"],
        ).drop(columns=["Date_of_report"])
        cache_dir and df.to_csv(cache_dir+"/rapport.csv", index=False)
    except:
        logging.warning("unable to download new data")
        df = cache_dir and pd.read_csv(cache_dir+"/rapport.csv", date_parser=[0]) or None
        
    return df


def post_image(figname):
    encoded_image = base64.b64encode(open(f"images/{figname}.jpg", "rb").read())
    res = requests.post(
        "https://api.imgur.com/3/image", data=encoded_image, headers=imgur_auth
    ).content
    return res and json.loads(res) or dict()


def check_posted_image(res):
    if res.get('success', False):
        return True
    else:
        error = res.get('data', '-')
        logging.warning(f'image not posted with {error}')
        return False


def add_img_block(figname, title=None, description=None, fig=None, retries=3):
    if fig is None:
        fig = plt.gcf()
    fig.savefig(f"images/{figname}.jpg", dpi=300)
    
    res = retry(post_image, check_posted_image, retries=5, sleep=10, figname=figname)
        
    url = res and res["data"]["link"] or None

    blocks = [
        title
        and {"type": "header", "text": {"type": "plain_text", "text": title}}
        or None,
        url and {"type": "image", "image_url": url, "alt_text": figname} or None,
        description
        and {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": description,
            },
        }
        or None,
    ]
    return list(filter(None, blocks))


def post_slack_update(slack_update):
    def _post_to_slack():
        return requests.post(slackhook, json={"blocks": slack_update})
    retry(
        _post_to_slack,
        lambda r: r.status_code == 200,
        None,
        10,
    )
