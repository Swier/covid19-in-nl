import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import pickle

smoothing = 7

rolling_kwargs = {
    "window": smoothing,
    "center": True,
    "min_periods": (smoothing - 1),
    "win_type": None,
}


    
def get_reproduction_number(df):
    return (
        df["Total_reported"]
        .rolling(7, center=True)
        .mean()
        .div(df["Total_reported"].rolling(7, center=True).mean().shift(4))
        .rolling(5)
        .mean()
    )


def get_latest_reproduction_number(df, smoothing=7):
    return (
        df["Total_reported"]
        .rolling(smoothing)
        .mean()
        .div(df["Total_reported"].rolling(smoothing).mean().shift(4))
        .dropna()
        .iloc[-1]
    )

def calculate_dagcorrectie(series, correction_start_date="2021-08-20"):
    return  np.exp(
        (
            np.log(series[correction_start_date:])
            - np.log(series[correction_start_date:])
            .rolling(**rolling_kwargs)
            .mean()
        )
        .groupby(series[correction_start_date:].index.weekday)
        .mean()
    )
    
def correct_weeksignaal(series, correction_start_date="2021-08-20"):
    correction = calculate_dagcorrectie(series, correction_start_date=correction_start_date)
    corrected = series.copy()
    corrected /= corrected.index.weekday.map(correction.to_dict())
    return corrected
    

def process_data(NL):
    corrected_opn, dag_correctie = correct_weeksignaal(NL)
    
    
def make_prognosis(corrected_opn, fit_weeks=2, lookahead=32):
    fit_period = fit_weeks * 2 + 1
    tmp = (
        np.log(corrected_opn).rolling(**rolling_kwargs).mean()[-fit_period:].dropna().copy()
    )
    vals = np.polyfit(range(len(tmp)), tmp, 1)
    tomorrow = corrected_opn.index.max() + pd.to_timedelta(1, "D")

    prognosis = np.exp(
        (
            pd.Series(
                range(prog_len + 1),
                index=pd.date_range(tomorrow, tomorrow + pd.to_timedelta(lookahead, "D")),
            )
            + fit_period
        )
        * vals[0]
        + vals[1]
    )
    return prognosis

def add_weektrend(series, dag_correctie):
    return  series * list(map(dag_correctie.get, df.index.weekday))


    
    
def build_slack_update(NL):
    slack_update = [
        {
            "type": "header",
            "text": {
                "type": "plain_text",
                "text": f'Corona update van {NL.iloc[-1].name.strftime("%Y-%m-%d")}',
            },
        },
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": (
                    f"Nieuwe besmettingen: *{NL.iloc[-1]['Total_reported']:.0f}*"
                    + "\n"
                    + f"Nieuwe ziekenhuisopnames: *{NL.iloc[-1]['Hospital_admission']:.0f}*"
                    + "\n"
                    + f"Nieuwe sterfgevallen: *{NL.iloc[-1]['Deceased']:.0f}*"
                ),
            },
        },
    ]
    