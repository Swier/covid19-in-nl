from covid import get_data, add_img_block, dag_correctie

from configfile import slackhook

data = get_data()

NL = df.groupby("Date_of_publication")[
    ["Total_reported", "Hospital_admission", "Deceased"]
].sum()

slack_update = [
    {
        "type": "header",
        "text": {
            "type": "plain_text",
            "text": f'Corona update van {NL.iloc[-1].name.strftime("%Y-%m-%d")}',
        },
    },
    {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": (
                f"Nieuwe besmettingen: *{NL.iloc[-1]['Total_reported']:.0f}*"
                + "\n"
                + f"Nieuwe ziekenhuisopnames: *{NL.iloc[-1]['Hospital_admission']:.0f}*"
                + "\n"
                + f"Nieuwe sterfgevallen: *{NL.iloc[-1]['Deceased']:.0f}*"
            ),
        },
    },
]

corrected_opn = dag_correctie()