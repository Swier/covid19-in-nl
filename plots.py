import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from covid import correct_weeksignaal

smoothing = 7

rolling_kwargs = {
    "window": smoothing,
    "center": True,
    "min_periods": (smoothing - 1),
    "win_type": None,
}

def plot_corrected(corrected_opn, NL, rolling_kwargs=rolling_kwargs):
    ax = corrected_opn.plot(
        figsize=(8, 5), label="Nieuwe besmettingen, gecorrigeerd", marker="."
    )

    corrected_opn.rolling(**rolling_kwargs).mean().plot(
        label=f"Nieuwe besmettingen ({smoothing}d gem.)", ax=ax
    )
    ax.set_title("Besmettingen zonder wekelijks signaal")
    ax.legend()
    ax.set_xlabel("Datum")
    ax.set_yscale("log")
    ax.grid(which="both", color="k", alpha=0.1)
    fig = plt.gcf()
    return fig

def plot_corrected_opnames(NL, start_date='2020-10-01', end_date=None, rolling_kwargs=rolling_kwargs):
    corrected_opn = correct_weeksignaal(NL['Total_reported'])[start_date:end_date]
    
    
    ax = corrected_opn.plot(
        figsize=(8, 5), label="Nieuwe besmettingen, gecorrigeerd", marker="."
    )

    corrected_opn.rolling(**rolling_kwargs).mean().plot(
        label=f"Nieuwe besmettingen ({smoothing}d gem.)", ax=ax
    )
    ax.set_title("Besmettingen zonder wekelijks signaal")
    ax.legend()
    ax.set_xlabel("Datum")
    ax.set_yscale("log")
    ax.grid(which="both", color="k", alpha=0.1)
    fig = plt.gcf()
    return fig

# fig = plot_corrected(corrected_opn, NL)

# figname = "besmettingen"
# title = "Nieuwe besmettingen"
# descr = (
#     "Nieuw vastgestelde besmettingen waarbij het wekelijks signaal eruit is gefilterd.\n"
#     "In het grijs zijn de nieuwe besmettingen zonder filtering te zien.\n"
#     "Let op, de verticale as is op logaritmische schaal."
# )
# prognose_corrected.plot(
#     ax=fig.get_axes()[0],
#     label=f"Prognose (R-waarde {(vals[0] + 1) ** 4:.2f})",
#     color="red",
#     alpha=0.5,
# )
# prognose_corrected_1w.plot(
#     ax=fig.get_axes()[0],
#     label=f"Prognose (o.b.v. 1 week, R-waarde {(vals_1w[0] + 1) ** 4:.2f})",
#     color="red",
#     style="--",
#     alpha=0.5,
# )
# plt.legend()

# slack_update += add_img_block(figname, title, descr)