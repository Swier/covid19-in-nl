# Covid-19 in Nederland

Dit notebook toont het aantal Covid-19 besmettingen in Nederland, en fit hier een exponentiële functie aan.

Let op: 
- Deze grafiek is niet door een expert gemaakt, deze informatie is niet beter dan die van het RIVM.
- Het verspreidingspatroon van een virus volgt alleen in de beginstadia een exponentiële functie.
- Effecten van meer/betere tests, betere voorlichting, etc. hebben invloed op de verspreiding, deze worden _niet_ meegenomen.